package com.ecommerceApplication.customerService.service;

import com.ecommerceApplication.customerService.entity.Customer;
import com.ecommerceApplication.customerService.repository.CustomerRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class CustomerService {


    @Autowired
    private CustomerRepository customerRepository;

    public  Customer saveCustomer(Customer customer) {
        log.info("saveCustomer method of CustomerRepository");
        return  customerRepository.save(customer);
    }


    public  Customer findByCustomerId(Long customerId) {
       log.info("findByCustomerId method of CustomerRepository");
        return  customerRepository.findByCustomerId(customerId);
    }
}
