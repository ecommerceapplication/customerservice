package com.ecommerceApplication.customerService.controller;

import com.ecommerceApplication.customerService.entity.Customer;
import com.ecommerceApplication.customerService.service.CustomerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/customer")
@Slf4j
public class CustomerController {

    @Autowired
    private CustomerService customerService;
    @PostMapping("/")
    public Customer saveCustomer(@RequestBody Customer customer)
    {

        log.info("Save customer method of CustomerController");
         return customerService.saveCustomer(customer);
    }
    @GetMapping("/{id}")
    public Customer findByCustomerId(@PathVariable("id") Long customerId)
    {
        log.info("get customerByID method of CustomerController");
        return customerService.findByCustomerId(customerId);
    }
}
